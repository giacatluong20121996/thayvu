package spring.api.model;

import spring.api.entity.Account;
import spring.api.entity.Folder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 12/13/2018.
 */
public class AccountModel {

    private String username;

    private List<Long> folders;

    public AccountModel(Account account) {
        username = account.getUsername();
        folders = new ArrayList<>();

        for (Folder folder: account.getFolders()) {
            folders.add(folder.getFolderId());
        }
    }

    public AccountModel(String username, List<Long> folders) {
        this.username = username;
        this.folders = folders;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Long> getFolders() {
        return folders;
    }

    public void setFolders(List<Long> folders) {
        this.folders = folders;
    }
}
