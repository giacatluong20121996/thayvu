package spring.api.model;

import spring.api.entity.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giaCatLuong on 12/13/2018.
 */
public class DataModel implements Comparable<DataModel>{
    private Long dataId;

    private String link;

    //private Double khongGian;
    //private Double phucVu;
    //private Double viTri;
    private Double giaCa;
    private Double chatLuong;
    private Double trungBinh;

    //private Long binhLuan;

    private String giaTien;
    private String duong;
    private String quan; // haiChau...
    private String linhVuc;
    private String hinhAnh;
    private Long maxPrice;
    private Long minPrice;

    public DataModel() {
    }

    public DataModel(Data data) {
        this.dataId = data.getDataId();
        this.link = data.getLink();
        //this.khongGian = data.getKhongGian();
        //this.phucVu = data.getPhucVu();
        //this.viTri = data.getViTri();
        // this.giaCa = data.getGiaCa();
        this.chatLuong = data.getChatLuong();
        this.trungBinh = data.getTrungBinh();
        //this.binhLuan = data.getBinhLuan();
        this.giaTien = data.getGiaTien();
        this.duong = data.getDuong();
        this.linhVuc = data.getLinhVuc();
        this.hinhAnh = data.getImage();
        this.quan = data.getQuan();
        this.maxPrice = data.getMaxPrice();
        this.minPrice = data.getMinPrice();
    }

    public Long getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Long getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Long minPrice) {
        this.minPrice = minPrice;
    }

    public DataModel(String link) {
        this.link = link;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

//    public Double getKhongGian() {
//        return khongGian;
//    }
//
//    public void setKhongGian(Double khongGian) {
//        this.khongGian = khongGian;
//    }
//
//    public Double getPhucVu() {
//        return phucVu;
//    }
//
//    public void setPhucVu(Double phucVu) {
//        this.phucVu = phucVu;
//    }
//
//    public Double getViTri() {
//        return viTri;
//    }
//
//    public void setViTri(Double viTri) {
//        this.viTri = viTri;
//    }

    public Double getGiaCa() {
        return giaCa;
    }

    public void setGiaCa(Double giaCa) {
        this.giaCa = giaCa;
    }

    public Double getChatLuong() {
        return chatLuong;
    }

    public void setChatLuong(Double chatLuong) {
        this.chatLuong = chatLuong;
    }

    public Double getTrungBinh() {
        return trungBinh;
    }

    public void setTrungBinh(Double trungBinh) {
        this.trungBinh = trungBinh;
    }

//    public Long getBinhLuan() {
//        return binhLuan;
//    }
//
//    public void setBinhLuan(Long binhLuan) {
//        this.binhLuan = binhLuan;
//    }

    public String getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(String giaTien) {
        this.giaTien = giaTien;
    }

    public String getDuong() {
        return duong;
    }

    public void setDuong(String duong) {
        this.duong = duong;
    }

    public String getQuan() {
        return quan;
    }

    public void setQuan(String quan) {
        this.quan = quan;
    }

    public String getLinhVuc() {
        return linhVuc;
    }

    public void setLinhVuc(String linhVuc) {
        this.linhVuc = linhVuc;
    }

    public String getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(String hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    @Override
    public int compareTo(DataModel o) {
        if (this.getTrungBinh() > o.getTrungBinh()) {
            return 1;
        }
        if (this.getTrungBinh() < o.getTrungBinh()) {
            return -1;
        }
        return 0;
    }

    public static class Builder {
        DataModel data;

        public Builder() {
            data = new DataModel();
        }

        public Builder setLink(String link) {
            data.link = link;
            return this;
        }

//        public Builder setKhongGian(Double khongGian) {
//            data.khongGian = khongGian;
//            return this;
//        }
//
//        public Builder setPhucVu(Double phucVu) {
//            data.phucVu = phucVu;
//            return this;
//        }
//
//        public Builder setViTri(Double viTri) {
//            data.viTri = viTri;
//            return this;
//        }

        public Builder setGiaCa(Double giaCa) {
            data.giaCa = giaCa;
            return this;
        }

        public Builder setChatLuong(Double chatLuong) {
            data.chatLuong = chatLuong;
            return this;
        }

        public Builder setTrungBinh(Double trungBinh) {
            data.trungBinh = trungBinh;
            return this;
        }

//        public Builder setBinhLuan(Long binhLuan) {
//            data.binhLuan = binhLuan;
//            return this;
//        }

        public Builder setGiaTien(String giaTien) {
            data.giaTien = giaTien;
            return this;
        }

        public Builder setDuong(String duong) {
            data.duong = duong;
            return this;
        }

        public Builder setQuan(String quan) {
            data.quan = quan;
            return this;
        }

        public Builder setLinhVuc(String linhVuc) {
            data.linhVuc = linhVuc;
            return this;
        }

        public DataModel build() {
            return data;
        }
    }

    public static List<DataModel> parseToModel(List<Data> datas) {
        List<DataModel> dataModels = new ArrayList<>();

        for (Data data: datas) {
            dataModels.add(new DataModel(data));
        }

        return dataModels;
    }

}
