package spring.api.model;

import spring.api.entity.Folder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giaCatLuong on 12/13/2018.
 */
public class FolderModel {

    private Long folderId;
    private String name;

    public FolderModel(Folder folder) {
        this.folderId = folder.getFolderId();
        this.name = folder.getName();
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<FolderModel> parseToModel(List<Folder> folders) {
        List<FolderModel> folderModels = new ArrayList<>();

        for (Folder folder: folders) {
            folderModels.add(new FolderModel(folder));
        }

        return folderModels;
    }
}
