package spring.api.util.File;

import java.io.File;

/**
 * Created by giaCatLuong on 12/10/2018.
 */
public class FileUtil {
    public static String dataLink = "." + concate("src", "main", "resources", "config"); //"/src/main/resources/config/data1.csv";

    static {
        //data = concate("src", "main", "resources", "config", "data1.csv");
    }
    public static final String SPERATOR = "/";
    public static final String CSV_SPLIT = ",";

    private static String concate(Object... objects) {
        StringBuilder log = new StringBuilder();

        for (Object object: objects) {
            log.append(SPERATOR + object);
        }

        return log + "";
    }

    public static void printSourceTree() {
        File file = new File("./src");
        for(String fileNames : file.list()) System.out.println(fileNames);
    }

    public static String convertFromUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    // convert from internal Java String format -> UTF-8
    public static String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
}
