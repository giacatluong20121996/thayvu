package spring.api.util.File;

import com.google.gson.Gson;
import spring.api.util.MyLogging;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by giaCatLuong on 12/10/2018.
 */
public class FileCreate {

    private static final char DEFAULT_SEPARATOR = ',';
    private static final char DEFAULT_QUOTE = '"';

        public static void readAndCreate(String url) {

            // FileUtil.printSourceTree();

            String csvFile = url;

            Scanner scanner = null;
            try {
                scanner = new Scanner(new File(csvFile));
                long lineTh = 1;
                while (scanner.hasNext()) {

                    String currentLine = scanner.nextLine();

                    MyLogging.getInstance().info(currentLine);

//                    if ( currentLine.indexOf(".jpg")== -1) {
//                        continue;
//                    }

                    List<String> line = parseLine(currentLine);

                    MyLogging.getInstance().info(lineTh++ + "",new Gson().toJson(line));
                }
                MyLogging.getInstance().info(lineTh++ + "end reading");
                scanner.close();
            } catch (FileNotFoundException e) {
                MyLogging.getInstance().error(e.getMessage());
            }


        }

        public static List<String> parseLine(String cvsLine) {
            return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
        }

        public static List<String> parseLine(String cvsLine, char separators) {
            return parseLine(cvsLine, separators, DEFAULT_QUOTE);
        }

        public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

            List<String> result = new ArrayList<>();

            //if empty, return!
            if (cvsLine == null && cvsLine.isEmpty()) {
                return result;
            }

            if (customQuote == ' ') {
                customQuote = DEFAULT_QUOTE;
            }

            if (separators == ' ') {
                separators = DEFAULT_SEPARATOR;
            }

            StringBuffer curVal = new StringBuffer();
            boolean inQuotes = false;
            boolean startCollectChar = false;
            boolean doubleQuotesInColumn = false;

            char[] chars = cvsLine.toCharArray();

            for (char ch : chars) {

                if (inQuotes) {
                    startCollectChar = true;
                    if (ch == customQuote) {
                        inQuotes = false;
                        doubleQuotesInColumn = false;
                    } else {

                        //Fixed : allow "" in custom quote enclosed
                        if (ch == '\"') {
                            if (!doubleQuotesInColumn) {
                                curVal.append(ch);
                                doubleQuotesInColumn = true;
                            }
                        } else {
                            curVal.append(ch);
                        }

                    }
                } else {
                    if (ch == customQuote) {

                        inQuotes = true;

                        //Fixed : allow "" in empty quote enclosed
                        if (chars[0] != '"' && customQuote == '\"') {
                            curVal.append('"');
                        }

                        //double quotes in column will hit this!
                        if (startCollectChar) {
                            curVal.append('"');
                        }

                    } else if (ch == separators) {

                        result.add(curVal.toString());

                        curVal = new StringBuffer();
                        startCollectChar = false;

                    } else if (ch == '\r') {
                        //ignore LF characters
                        continue;
                    } else if (ch == '\n') {
                        //the end, break!
                        break;
                    } else {
                        curVal.append(ch);
                    }
                }

            }

            result.add(curVal.toString());

            return result;
    }
}
