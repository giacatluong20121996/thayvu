package spring.api.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by giaCatLuong on 12/10/2018.
 */
public class MyLogging {

    private Logger logger;
    private static MyLogging instance = new MyLogging();

    public static MyLogging getInstance() {
        return instance;
    }

    private MyLogging() {
        logger = LoggerFactory.getLogger("myLogging");
    }

    public void info(Object... objects) {
        logger.info(concate(objects));
    }

    public void error(Object... objects) {
        logger.error(concate(objects));
    }

    private String concate(Object... objects) {
        StringBuilder log = new StringBuilder();

        for (Object object: objects) {
            log.append(object + " | ");
        }

        return log + "";
    }
}
