package spring.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import spring.api.entity.Data;
import spring.api.repository.AccountRepository;
import spring.api.repository.DataRepository;
import spring.api.repository.FolderRepository;
import spring.api.service.FolderService;
import spring.api.util.File.FileUtil;
import spring.api.util.MyLogging;

import java.io.*;
import java.util.Scanner;


@SpringBootApplication()
public class ApiApplication implements CommandLineRunner{

	public static final boolean IS_INIT_DATA = true;
	public static final boolean IS_HANDLE_CONFIG = true;
	public static final boolean IS_CLEAR_DATA = true;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private FolderRepository folderRepository;

	@Autowired
	private DataRepository dataRepository;

	@Autowired
	private FolderService folerSevice;

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

	public static void handleReadingConfig(String url) {
		try {
			MyLogging.getInstance().info("start handle reading file");
			// FileCreate.readAndCreate(FileUtil.dataLink);

			Scanner scanner = new Scanner(new File(url));
			Scanner dataScanner = null;

			int index = 0;
			int pos = 0;

			while (scanner.hasNextLine()) {
				dataScanner = new Scanner(scanner.nextLine());
				dataScanner.useDelimiter(",");

				System.out.print("\npos " + pos++ + " ");

				while (dataScanner.hasNext()) {
					String data = dataScanner.next();
					System.out.print(index++ + " invalid data::");
					index++;
				}
				index = 0;
			}

			System.out.print("\nend");

			scanner.close();

		} catch (Exception e) {
			MyLogging.getInstance().error(e.getMessage());
		}
	}

	// test
	@Override
	public void run(String... args) throws Exception {
		readFile("data22.csv");
		MyLogging.getInstance().info("data22");
		readFile("data21.csv");
		MyLogging.getInstance().info("data21");
		readFile("data12.csv");
		MyLogging.getInstance().info("data12");
		readFile("data11.csv");
		MyLogging.getInstance().info("data11");
	}
	public void readFile(String url) throws FileNotFoundException{

		MyLogging.getInstance().info("start fucking reading");

		File file = new File(FileUtil.dataLink + "/" + url);

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		try {
			int i = 0;

			br.readLine();

			while ((st = br.readLine()) != null) {

				System.out.println(st);

				if (st.substring(0, 3).equals(",,,")) {
					continue;
				}

				String[] rawValues = st.split(",");
				if (rawValues.length < 2) {
					continue;
				} else {

//					String[] minMax = rawValues[8].split("Ä‘ - ");
//
//					System.out.println(rawValues[8] + " length " + minMax.length + ", " + minMax[0] + ", " + minMax[1]);
//
//					for (int ii = 0; i < minMax.length; i++) {
//						System.out.print(ii);
//					}
//
//					System.out.println("ffffffff");

//					int min = Integer.parseInt(minMax[0].split("\\.")[0]);
//					int max = Integer.parseInt(minMax[1].split("\\.")[0]);


//					for (int i = 0; i < rawValues; i++) {
//
//					}

					int min = (int) (Math.random() * 10 + 15);
					int max = (int) (Math.random() * 50 + min);

					String quan = rawValues[10];

//					String quan = FileUtil.convertFromUTF8(rawValues[10]);
//
//					if (quan.substring(0, 8).equals("Quận Cẩm")) {
//						quan = "quan cam le";
//					} else if (quan.substring(0,9).equals("Quận Liên")) {
//						quan = "quan lien chieu";
//					} else if (quan.substring(0, 6).equals("Quận S")) {
//						quan = "quan son tra";
//					} else if (quan.substring(0, 6).equals(("Quận T"))) {
//						quan = "quan thanh khe";
//					}

					System.out.println("aaa " + quan);

					Data data1 = new Data.Builder()
							.setKhongGian(Double.parseDouble(rawValues[0]))
							.setPhucVu(Double.parseDouble(rawValues[1]))
							.setViTri(Double.parseDouble(rawValues[2]))
							.setGiaCa(Double.parseDouble(rawValues[3]))
							.setChatLuong(Double.parseDouble(rawValues[4]))
							.setTrungBinh(Double.parseDouble(rawValues[5]))
							.setBinhLuan(Long.parseLong(rawValues[6]))
							.setThoiGian(rawValues[7])
							.setGiaTien(rawValues[8])
							.setDuong(rawValues[9])
							.setQuan(quan)
							.setAmThuc(rawValues[11])
							.setImage(rawValues[12])
							.setViDo(Double.parseDouble(rawValues[13]))
							.setKinhDo(Double.parseDouble(rawValues[14]))
							.setLink(rawValues[15])
							.setMaxPrice(max * 1000)
							.setMinPrice(min * 1000)
							.setLinhVuc(st.length() > 18 ? "0" : "1")
							.build();
					dataRepository.save(data1);

				}
				MyLogging.getInstance().info("pos " + i++);
			}

			MyLogging.getInstance().info("okkkkkkkkkkk reading");

		} catch (Exception e) {
			e.getMessage();
		} finally {
			{
				try {
					if (br != null)
						br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
