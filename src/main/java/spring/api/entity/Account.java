package spring.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by giaCatLuong on 12/10/2018.
 */
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(generator = "accountId")
    @GenericGenerator(name = "accountId", strategy = "increment")
    private Long accountId;

    @Column(unique = true)
    private String username;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "account")
    private Set<Folder> folders= new HashSet<>();

    public Account() {
    }

    public Account(String username) {
        this.username = username;
    }


    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<Folder> getFolders() {
        return folders;
    }

    public void setFolders(Set<Folder> folders) {
        this.folders = folders;
    }
}
