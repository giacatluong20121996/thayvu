package spring.api.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by giaCatLuong on 12/10/2018.
 */

@Entity
@Table(name = "data")
public class Data {

    @Id
    @GeneratedValue(generator = "dataId")
    @GenericGenerator(name = "dataId", strategy = "increment")
    private Long dataId;

    @Column(unique = false)
    private String link;

    private Double khongGian;
    private Double phucVu;
    private Double viTri;
    private Double giaCa;
    private Double chatLuong;
    private Double trungBinh;

    private Long minPrice;
    private Long maxPrice;

    private Long binhLuan;

    private String giaTien;
    private String duong;
    private String quan; // haiChau...
    private String linhVuc;
    private String hinhAnh;
    private String amThuc;
    private String image;
    private double viDo;
    private double kinhDo;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "datas") // xem lai ALL
    private Set<Folder> folders = new HashSet<>();

    public Data() {
    }

    public Long getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Long minPrice) {
        this.minPrice = minPrice;
    }

    public Long getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Data(String link) {
        this.link = link;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Double getKhongGian() {
        return khongGian;
    }

    public void setKhongGian(Double khongGian) {
        this.khongGian = khongGian;
    }

    public Double getPhucVu() {
        return phucVu;
    }

    public void setPhucVu(Double phucVu) {
        this.phucVu = phucVu;
    }

    public Double getViTri() {
        return viTri;
    }

    public void setViTri(Double viTri) {
        this.viTri = viTri;
    }

    public Double getGiaCa() {
        return giaCa;
    }

    public void setGiaCa(Double giaCa) {
        this.giaCa = giaCa;
    }

    public Double getChatLuong() {
        return chatLuong;
    }

    public void setChatLuong(Double chatLuong) {
        this.chatLuong = chatLuong;
    }

    public Double getTrungBinh() {
        return trungBinh;
    }

    public void setTrungBinh(Double trungBinh) {
        this.trungBinh = trungBinh;
    }

    public Long getBinhLuan() {
        return binhLuan;
    }

    public void setBinhLuan(Long binhLuan) {
        this.binhLuan = binhLuan;
    }

    public String getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(String giaTien) {
        this.giaTien = giaTien;
    }

    public String getDuong() {
        return duong;
    }

    public void setDuong(String duong) {
        this.duong = duong;
    }

    public String getQuan() {
        return quan;
    }

    public void setQuan(String quan) {
        this.quan = quan;
    }

    public String getLinhVuc() {
        return linhVuc;
    }

    public void setLinhVuc(String linhVuc) {
        this.linhVuc = linhVuc;
    }

    public String getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(String hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    public Set<Folder> getFolders() {
        return folders;
    }

    public void setFolders(Set<Folder> folders) {
        this.folders = folders;
    }

    public String getAmThuc() {
        return amThuc;
    }

    public void setAmThuc(String amThuc) {
        this.amThuc = amThuc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getViDo() {
        return viDo;
    }

    public void setViDo(double viDo) {
        this.viDo = viDo;
    }

    public double getKinhDo() {
        return kinhDo;
    }

    public void setKinhDo(double kinhDo) {
        this.kinhDo = kinhDo;
    }

    public static class Builder {
        Data data;

        public Builder() {
            data = new Data();
        }

        public Builder setLink(String link) {
            data.link = link;
            return this;
        }

        public Builder setKhongGian(Double khongGian) {
            data.khongGian = khongGian;
            return this;
        }

        public Builder setPhucVu(Double phucVu) {
            data.phucVu = phucVu;
            return this;
        }

        public Builder setViTri(Double viTri) {
            data.viTri = viTri;
            return this;
        }

        public Builder setGiaCa(Double giaCa) {
            data.giaCa = giaCa;
            return this;
        }

        public Builder setChatLuong(Double chatLuong) {
            data.chatLuong = chatLuong;
            return this;
        }

        public Builder setTrungBinh(Double trungBinh) {
            data.trungBinh = trungBinh;
            return this;
        }

        public Builder setBinhLuan(Long binhLuan) {
            data.binhLuan = binhLuan;
            return this;
        }

        public Builder setGiaTien(String giaTien) {
            data.giaTien = giaTien;
            return this;
        }

        public Builder setDuong(String duong) {
            data.duong = duong;
            return this;
        }

        public Builder setQuan(String quan) {
            data.quan = quan;
            return this;
        }

        public Builder setLinhVuc(String linhVuc) {
            data.linhVuc = linhVuc;
            return this;
        }

        public Builder setMaxPrice(long maxPrice) {
            data.maxPrice = maxPrice;
            return this;
        }

        public Builder setMinPrice(long minPrice) {
            data.minPrice = minPrice;
            return this;
        }

        public Builder setAmThuc(String amThuc) {
            data.amThuc = amThuc;
            return this;
        }

        public Builder setImage(String image) {
            data.image = image;
            return this;
        }

        public Builder setViDo(Double viDo) {
            data.viDo = viDo;
            return this;
        }

        public Builder setThoiGian(String a) {
            return this;
        }

        public Builder setKinhDo(Double kinhDo) {
            data.kinhDo = kinhDo;
            return this;
        }

        public Data build() {
            return data;
        }
    }

    public String toString() {
        String result = String.format(
                "Data [id=%d, link='%s']%n",
                dataId, link);
//        if (datas != null) {
//            for(Data data : datas) {
//                result += String.format(
//                        "Data[id=%d, name='%s']%n",
//                        data.getDataId(), data.getLink());
//            }
//        }

        return result;
    }

}
