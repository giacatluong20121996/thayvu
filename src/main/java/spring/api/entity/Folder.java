package spring.api.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by giaCatLuong on 12/10/2018.
 * https://www.callicoder.com/hibernate-spring-boot-jpa-many-to-many-mapping-example/
 */
@Entity
@Table(name = "folder", uniqueConstraints=
@UniqueConstraint(columnNames={"folderId", "name"})
)
public class Folder {

    @Id
    @GeneratedValue(generator = "folderId")
    @GenericGenerator(name = "folderId", strategy = "increment")
    private Long folderId;

    @Column(unique = true)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}) // xem lai ALL
    //The entity that specifies the @JoinTable is the owning side of the relationship and the entity that specifies the mappedBy attribute is the inverse side.
    @JoinTable(name = "folder_data", joinColumns = {@JoinColumn(name = "foldersId", referencedColumnName = "folderId")}, inverseJoinColumns = {@JoinColumn(name = "datasId", referencedColumnName = "dataId")})
//    @JsonBackReference
    private Set<Data> datas = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", nullable = false)
    private Account account;

    public Folder() {}

    public Folder(String name, Account account) {
        this.name = name;
        this.account = account;
    }

    public Set<Data> getDatas() {
        return datas;
    }

    public void setDatas(Set<Data> datas) {
        this.datas = datas;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = String.format(
                "Folder [id=%d, name='%s']%n",
                folderId, name);
        if (datas != null) {
            for(Data data : datas) {
                result += String.format(
                        "Data[id=%d, name='%s']%n",
                        data.getDataId(), data.getLink());
            }
        }

        return result;
    }
}
