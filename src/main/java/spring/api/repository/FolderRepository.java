package spring.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import spring.api.entity.Account;
import spring.api.entity.Folder;

import java.util.List;

/**
 * Created by giaCatLuong on 12/10/2018.
 */
@Repository
public interface FolderRepository extends JpaRepository<Folder, Long>{
    List<Folder> findByAccountOrderByFolderIdAsc(Account account);

//    @Modifying
//    @Transactional
//    @Query("delete from Folder u where u.foderId = ?1")
//    void deleteFolderByFolderId(Long folderId);


    @Transactional
    public void removeByFolderId(Long folderId);

    @Transactional
    List<Folder> findByFolderId(Long folderId);

    @Transactional
    @Query
    public void deleteByAccount(Account account);
}
