package spring.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.api.entity.Data;

/**
 * Created by giaCatLuong on 12/10/2018.
 */
@Repository
public interface DataRepository extends JpaRepository<Data, Long> {

}
