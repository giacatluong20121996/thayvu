package spring.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.api.entity.Account;

/**
 * Created by Admin on 12/11/2018.
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{
    Account findByUsername(String username);
}
