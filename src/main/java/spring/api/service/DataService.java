package spring.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.api.model.DataModel;
import spring.api.repository.DataRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giaCatLuong on 12/11/2018.
 */
@Service
public class DataService {

    @Autowired
    DataRepository dataRepository;

    public List<DataModel> getDataByName(String dataName) {

        List<DataModel> datas = DataModel.parseToModel(dataRepository.findAll());

        dataName = dataName.toLowerCase();

        int lengthOfSearchingString = dataName.length();
        int currentLength = lengthOfSearchingString / 2 + 1;
        int maxNumberOfResult = 10;
        int index = -1;

        int[] lengthOfCommonSubstring = new int[maxNumberOfResult];

        DataModel[] datasSearched = new DataModel[maxNumberOfResult];


        for (DataModel city : datas) {

            String stringCompared = city.getDuong().toLowerCase();

            int tmp = longestCommonSubstringTabutation(dataName, stringCompared);

            if (tmp >= currentLength) {

                index++;

                datasSearched[index % maxNumberOfResult] = city;
                lengthOfCommonSubstring[index % maxNumberOfResult] = tmp;
                currentLength = tmp;

            }
        }

        for (int i = 0; i < maxNumberOfResult - 1; i++) {

            for (int j = 0; j < maxNumberOfResult - i - 1; j++) {

                if (lengthOfCommonSubstring[j] < lengthOfCommonSubstring[j + 1]) {

                    int tmp = lengthOfCommonSubstring[j];

                    lengthOfCommonSubstring[j] = lengthOfCommonSubstring[j + 1];
                    lengthOfCommonSubstring[j + 1] = tmp;

                    DataModel tmpCity = datasSearched[j];

                    datasSearched[j] = datasSearched[j + 1];
                    datasSearched[j + 1] = tmpCity;

                } else if (lengthOfCommonSubstring[j] == lengthOfCommonSubstring[j + 1]
                        && lengthOfCommonSubstring[j] != 0
                        && Math.abs(datasSearched[j].getDuong().length() - dataName.length()) > Math
                        .abs(datasSearched[j + 1].getDuong().length() - dataName.length())) {

                    int tmp = lengthOfCommonSubstring[j];

                    lengthOfCommonSubstring[j] = lengthOfCommonSubstring[j + 1];
                    lengthOfCommonSubstring[j + 1] = tmp;

                    DataModel tmpData = datasSearched[j];

                    datasSearched[j] = datasSearched[j + 1];
                    datasSearched[j + 1] = tmpData;

                }

            }

        }

        List<DataModel> result = new ArrayList<DataModel>();

        for (DataModel city : datasSearched) {

            if (city != null) {
                result.add(city);
            }

        }

        return result;
    }

    /**
     * lcs(str1, str2, lengthOfString1, lengthOfString2) = Max {
     *                               1 + lcs(str1, str2, lengthOfString1 - 1, lengthOfString2 - 2),
     *                               lcs(str1, str2, lengthOfString1 - 1, lengthOfString2),
     *                               lcs(str1, str2, lengthOfString1, lengthOfString2 - 1)
     *                               }
     *
     * @return longest common substring of two strings
     * @param string1
     * @param string2
     */

    int longestCommonSubstringTabutation(String string1, String string2) {

        int[][] memoization = new int[string1.length()][string2.length()];

        int lengthOfString1 = string1.length();
        int lengthOfString2 = string2.length();

        for (int i = 0; i < lengthOfString1; i++) {

            for (int j = 0; j < lengthOfString2; j++) {

                if (string1.charAt(i) == string2.charAt(j)) {

                    if (i != 0 && j !=0 ) {

                        memoization[i][j] = 1 + memoization[i - 1][j - 1];

                    } else {

                        memoization[i][j] = 1;

                    }


                } else {

                    if (i != 0) {

                        memoization[i][j] = memoization[i - 1][j];

                    }

                    if (j != 0 && memoization[i][j] < memoization[i][j - 1]) {

                        memoization[i][j] = memoization[i][j - 1];

                    }

                }

            }

        }

        return memoization[lengthOfString1 - 1][lengthOfString2 - 1];

    }
}
