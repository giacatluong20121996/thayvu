package spring.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.api.entity.Account;
import spring.api.entity.Folder;
import spring.api.repository.AccountRepository;
import spring.api.repository.FolderRepository;

import java.util.List;

/**
 * Created by giaCatLuong on 12/11/2018.
 */
@Service
public class FolderService {

    @Autowired
    FolderRepository folderRepository;

    @Autowired
    AccountRepository accountRepository;

    public List<Folder> getAllFolders(String username) {
        Account account = accountRepository.findByUsername(username);
        return folderRepository.findByAccountOrderByFolderIdAsc(account);
    }

    // similar to update folder
    public void addFolder(String username, Folder folder) {
        Account account = accountRepository.findByUsername(username);
        folder.setAccount(account);
        folderRepository.save(folder);
    }

    public void updateFolder(String username, Folder folder) {
        Account account = accountRepository.findByUsername(username);
        folder.setAccount(account);
        folderRepository.save(folder);
    }

    public void deleteFolder(Long id) {
        folderRepository.removeByFolderId(id);
    }

    public List<Folder> findById(Long id) {
        return folderRepository.findByFolderId(id);
    }
}
