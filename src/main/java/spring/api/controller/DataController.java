package spring.api.controller;

import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.api.entity.Data;
import spring.api.model.DataModel;
import spring.api.repository.DataRepository;
import spring.api.service.AccountService;
import spring.api.service.DataService;
import spring.api.service.FolderService;
import spring.api.util.MyLogging;

import java.util.*;

/**
 * Created by giaCatLuong on 12/11/2018.
 */
@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
public class DataController {
    @Autowired
    AccountService accountService;

    @Autowired
    FolderService folderService;

    @Autowired
    DataRepository dataRepository;

    @Autowired
    DataService dataService;

    @RequestMapping(method = RequestMethod.GET, value = "/data")
    ResponseEntity<DataModel> getAllData() {

        List<Data> datas = dataRepository.findAll();

        List<DataModel> dataModels = DataModel.parseToModel(new ArrayList<>(datas));

        return new ResponseEntity(dataModels, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/searchdata")
    String getDatas() {

        List<Data> datas = dataRepository.findAll();
        List<DataModel> dataModels = DataModel.parseToModel(new ArrayList<>(datas));

        Collections.reverse(dataModels);

        return "{ \"data\":" +  new Gson().toJson(dataModels.subList(0, 10)) + "}";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/searchdata")
    String getDatas(@RequestBody String request) {

        List<Data> datas = dataRepository.findAll();

        List<DataModel> dataModels = DataModel.parseToModel(new ArrayList<>(datas));
        List<DataModel> results = findData(dataModels, request);

        return "{ \"data\": " +  new Gson().toJson(results) + " }";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/searchdatacontent")
    String getDatasContent(@RequestBody String request) {

        JSONObject jsonObject = new JSONObject(request);
        String content = jsonObject.getString("content");

        List<Data> datas = dataRepository.findAll();

        List<DataModel> dataModels = DataModel.parseToModel(new ArrayList<>(datas));
        List<DataModel> results = dataService.getDataByName(content);

        return "{ \"data\": " +  new Gson().toJson(results) + " }";
    }


    private List<DataModel> findData(List<DataModel> dataModels, String request) {
        List<DataModel> results = new ArrayList<>();

        Map<String, String> searching = readJson(request);
        Long maxPrice = Long.parseLong(searching.get("maxPrice"));
        Long minPrice = Long.parseLong(searching.get("minPrice"));

        for (DataModel dataModel: dataModels) {

            if (!dataModel.getQuan().equals(searching.get("khuvuc"))) {
                MyLogging.getInstance().info(dataModel.getQuan() + " khuvucccccccccccc " + searching.get("khuvuc"));
                continue;
            }

            if (maxPrice < dataModel.getMinPrice()) {
                MyLogging.getInstance().info("priceeeeeeeeeee");
                continue;
            }

            results.add(dataModel);

        }

        return results;
    }

    private Map<String, String> readJson(String request) {
        JSONObject jsonObject = new JSONObject(request);
        String khuvuc = jsonObject.getString("khuvuc");
        String maxPrice = jsonObject.getString("maxPrice");
        String minPrice = jsonObject.getString("minPrice");
        String loaiHinh = jsonObject.getString("loaihinh");
        String mucDich = jsonObject.getString("mucdich");

        Map<String, String> rs = new HashMap<>();
        rs.put("khuvuc", khuvuc);
        rs.put("maxPrice", maxPrice);
        rs.put("minPrice", minPrice);
        rs.put("loaiHinh", loaiHinh);
        rs.put("mucDich", mucDich);

        return rs;
    }
}
