package spring.api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by giaCatLuong on 12/11/2018.
 */
@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
public class AccountController {
}
