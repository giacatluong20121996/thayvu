package spring.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.api.entity.Folder;
import spring.api.model.DataModel;
import spring.api.model.FolderModel;
import spring.api.service.AccountService;
import spring.api.service.FolderService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giaCatLuong on 12/11/2018.
 */
@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
public class FolderController {

    @Autowired
    AccountService accountService;

    @Autowired
    FolderService folderService;

    @RequestMapping(method = RequestMethod.GET, value = "/folder")
    ResponseEntity<FolderModel> getAllFolders(@RequestHeader("Authorization") String token) {
        //todo tmp userName
        // String username = FacebookUtil.sendToFacebook(token);
        String username = "thaiVan";
        if (username == null) {
            // return new ResponseEntity(false, HttpStatus.EXPECTATION_FAILED);
        }

        List<FolderModel> result = FolderModel.parseToModel(folderService.getAllFolders(username));
        return new ResponseEntity(result, HttpStatus.OK);
        //return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/folder/{id}")
    ResponseEntity<DataModel> getALlDatasInFolder(@RequestHeader("Authorization") String token, @PathVariable Long id) {
        //todo tmp userName
        //String username = FacebookUtil.sendToFacebook(token);

        String username = "thaiVan";

        if (username == null) {
            // return new ResponseEntity(false, HttpStatus.EXPECTATION_FAILED);
        }

        Folder folder = folderService.findById(id).get(0);
        List<DataModel> dataModels = DataModel.parseToModel(new ArrayList<>(folder.getDatas()));
        return new ResponseEntity(dataModels, HttpStatus.OK);
        //return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/folder")
    ResponseEntity addFolder(@RequestHeader("Authorization") String token, @RequestBody Folder folder) {
        //todo tmp userName
        //String username = FacebookUtil.sendToFacebook(token);

        String username = "thaiVan";

        if (username == null) {
            // return new ResponseEntity(false, HttpStatus.EXPECTATION_FAILED);
        }

        folderService.addFolder(username, folder);
        return new ResponseEntity(true, HttpStatus.OK);
        //return null;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/folder")
    ResponseEntity updateFolder(@RequestHeader("Authorization") String token, @RequestBody Folder folder) {
        //todo tmp userName
        // String username = FacebookUtil.sendToFacebook(token);

        String username = "thaiVan";

        if (username == null) {
            // return new ResponseEntity(false, HttpStatus.EXPECTATION_FAILED);
        }

        folderService.updateFolder(username, folder);
        return new ResponseEntity(true, HttpStatus.OK);
        //return null;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/folder/{id}")
    ResponseEntity deleteFolder(@RequestHeader("Authorization") String token, @PathVariable Long id) {
        //todo tmp userName
        // String username = FacebookUtil.sendToFacebook(token);

        String username = "thaiVan";

        if (username == null) {
            // return new ResponseEntity(false, HttpStatus.EXPECTATION_FAILED);
        }

        folderService.deleteFolder(id);
        return new ResponseEntity(true, HttpStatus.OK);
        //return null;
    }

}
